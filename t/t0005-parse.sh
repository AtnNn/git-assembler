#!/bin/sh
DESC="parsing tests"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repository"
git init -q

verb "test reading .gitassembly with no eol"
printf "base test master" > .gitassembly
[ "$(gas)" = "test .. master" ]

