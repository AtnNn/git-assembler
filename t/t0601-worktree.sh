#!/bin/sh
DESC="basic worktree test"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and a worktree"
git init -q master
(
    cd master
    git commit --allow-empty -m 'first commit' -q
    git worktree add ../worktree -b worktree -q
)

verb "initialize assembly file"
cat <<EOF > master/.git/assembly
stage test worktree
EOF

verb "ensure gas works in master"
(
    cd master
    quiet gas -av
)

verb "add commit to worktree"
(
    cd worktree
    touch worktree_file
    git add worktree_file
    git commit -m 'worktree_file' -q
)

verb "ensure gas works in worktree"
(
    cd worktree
    quiet gas -av
)
